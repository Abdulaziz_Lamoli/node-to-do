const bodyParser = require('body-parser');
const Todo= require('../models/todoModel');

let urlencodedParser = bodyParser.urlencoded({ extended: false })


// routes creation
module.exports = function(app){

app.get('/todo', (req,res)=>{
    //get data from mongoDB and pass to view
    Todo.Todo.find({}, function(err, data){
        if (err) throw err;
        res.render('todo', {todos: data})
    })
    
});

app.post('/todo', urlencodedParser, (req,res)=>{
    // add data to mongoDB
    let newTodoItem = Todo.Todo(req.body).save(function(err, data){
        if(err) throw err;
        console.log(data);
        res.json(data);
    })

});

app.delete('/todo/:item', (req,res)=>{
    console.log(req.params.item);
    Todo.Todo.find({item: req.params.item.replace(/\-/g, " ")}).deleteOne(function(err, data){
        if(err) throw err;
        res.json(data);

    });


});


};