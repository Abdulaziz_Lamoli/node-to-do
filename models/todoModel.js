const mongoose = require('mongoose');

//connect to mongoDB
mongoose.connect('mongodb+srv://aziz:0099@todo-vjnnz.mongodb.net/test?retryWrites=true&w=majority', {useUnifiedTopology: true, useNewUrlParser: true});
//creat a schema 
let todoSchema = new mongoose.Schema({
    item: String
});

let Todo = mongoose.model('Todo', todoSchema);

module.exports = {
    Todo: Todo
};