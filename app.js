const express = require('express');
const todoController = require('./controllers/todoController');
const port = 3000;
let app = express();

//set temp engine
app.set('view engine','ejs');

//set up middleware in this case it is the entire public dir
app.use(express.static('./public'));

//fire controllers

todoController(app);

//listen to app and display sucess msg
app.listen(port, (success)=>{
    console.log('app is listening');
});
